import { LogicBase, logger } from "bondage-club-bot-api";

export class LoggingLogic extends LogicBase {
    protected onMessage(connection: API_Connector, message: BC_Server_ChatRoomMessage, sender: API_Character): void {
        const dict = message.Dictionary === undefined ? "" : `; dict: ${JSON.stringify(message.Dictionary)}`;
        if (message.Type !== "Hidden") {
            logger.info(`Message ${message.Type} from ${sender.Name} (${sender.MemberNumber}): ` + `${message.Content}${dict}`)
        }
    }

    protected onCharacterLeft(connection: API_Connector, character: API_Character, intentional: boolean): void {
        logger.info(
			`Character ${character.Name} (${character.MemberNumber}) ` +
			`left room ${connection.chatRoom.Name}, intentional: ${intentional}`
		);
    }

    protected onCharacterEntered(connection: API_Connector, character: API_Character): void {
		logger.info(`Character ${character.Name} (${character.MemberNumber}) entered room ${connection.chatRoom.Name}`);
	}

    protected onCharacterOrderChanged(connection: API_Connector): void {
		logger.info(`Character order inside room ${connection.chatRoom.Name} changed`);
	}

    protected onCharacterMetadataChanged(connection: API_Connector, character: API_Character, joining: boolean): void {
		if (joining) return;
		logger.info(`Character ${character.Name} (${character.MemberNumber}) metadata changed`);
	}

    protected onRoomForceLeave(connection: API_Connector, type: "Kicked" | "Banned"): void {
		logger.error(`${type} from room`);
	}

    protected onRoomUpdate(connection: API_Connector, sourceMemberNumber: number): void {
		logger.info(`Chatroom ${connection.chatRoom.Name} updated by ${sourceMemberNumber}`);
	}

    protected onCharacterEvent(connection: API_Connector, event: AnyCharacterEvent): void {
		switch (event.name) {
			case "ItemAdd": {
				if (event.initial) return;
				const source = event.character === event.source ? "" : ` from ${event.source.Name} (${event.source.MemberNumber})`;
				logger.info(
					`ItemAdd for ${event.character.Name} (${event.character.MemberNumber})${source}: ${event.item.Group}:${event.item.Name}`
				);
				break;
			}
			case "ItemChange": {
				if (event.initial) return;
				const source = event.character === event.source ? "" : ` from ${event.source.Name} (${event.source.MemberNumber})`;
				logger.info(
					`ItemChange for ${event.character.Name} (${event.character.MemberNumber})${source}: ${event.item.Group}:${event.item.Name}`
				);
				break;
			}
			case "ItemRemove": {
				const source = event.character === event.source ? "" : ` from ${event.source.Name} (${event.source.MemberNumber})`;
				logger.info(
					`ItemRemove for ${event.character.Name} (${event.character.MemberNumber})${source}: ${event.item.Group}:${event.item.Name}`
				);
				break;
			}
			case "PoseChanged":
				logger.info(`PoseChanged for ${event.character.Name}: ${event.character.Pose.map(P => P.Name)}`);
				break;
			case "SafewordUsed":
				logger.alert(`Character ${event.character.Name} used swafeword! (full release: ${event.release})`);
				break;
			default:
				// @ts-expect-error: We shouldn't reach this
				logger.info(`Unknown character event ${event.name} from ${event.character.Name}`);
				break;
		}
	}

    protected onBotEvent(connection: API_Connector, event: AnyBotEvent): void {
		if (event.name === "Message") {
			const dict = event.Dictionary == null ? "" : `; dict: ${JSON.stringify(event.Dictionary)}`;
			const tc = event.Target !== null && connection.chatRoom.characters.find(c => c.MemberNumber === event.Target);
			const target = event.Target === null ? "" : `to ${tc || event.Target} `;
			if (event.Type !== "Hidden") {
				const msg = `Bot "${connection.Player.Name}" message ${event.Type} ` +
					target +
					`: ` +
					`${event.Content}${dict}`;
				//logger.debug(msg);
			}
		}
	}
}