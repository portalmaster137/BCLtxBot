import { AssetGet, BC_PermissionLevel, logger } from "bondage-club-bot-api";
import { LoggingLogic } from "./logging";

class State {
    state: number = 0;
    allow_use: boolean = true;
}

export class MainLogic extends LoggingLogic {
    connections = new Map<API_Character, State>();

    constructor(){
        super();
        this.connections = new Map();
    }

    protected onCharacterEntered(connection: API_Connector, character: API_Character): void {
        super.onCharacterEntered(connection, character);
        this.connections.set(character, new State());

        character.Tell("Emote", `*Welcome ${character.Name}, as you enter the room, you see a control panel. (touch) the panel?`);
		character.Tell("Emote", `*(Whisper commands in (brackets) to the bot with ! before the command, such as (touch) would be !touch)`);

        //DEBUG: logger.info("State setup for " + character.Name);
		if (character.ItemPermission === 0 || character.ItemPermission === 1) {

		} else {
			character.Tell("Emote", `*Due to your Item Permissions, you won't be able to do anything, try changing them and rejoining.`);
			this.connections.get(character)!.allow_use = false;
		}
		//

    }

    protected onCharacterLeft(connection: API_Connector, character: API_Character, intentional: boolean): void {
        this.connections.delete(character);
    }

    protected onMessage(connection: API_Connector, message: BC_Server_ChatRoomMessage, sender: API_Character): void {
        super.onMessage(connection, message, sender);
        const state = this.connections.get(sender);
        if (state === undefined) {
            logger.error("State not found for " + sender.Name + ". This should never happen.");
            return;
        }
        //DEBUG: logger.info("State for " + sender.Name + " is " + state.state);

        if (message.Type === "Whisper" && message.Content.startsWith("!")) {
			const cmd = message.Content.toLocaleLowerCase();
			if (!state.allow_use) {
				sender.Tell("Whisper", `You can't use the panel due to your Item Permissions.`);
				return;
			}
			if (cmd === "!debug") {
				sender.Tell("Whisper", state.state.toString());
				sender.Tell("Whisper", state.allow_use.toString());
				return;
			}

			if (cmd === "!touch" && state.state === 0) {
				sender.Tell("Emote", `*You touch the panel and it lights up.`);
				sender.Tell("Emote", `*The panel has a button that says "Register". (press) the button?`);
				state.state = 1;
				return;
			}

			if (cmd === "!press" && state.state === 1) {
				sender.Tell("Emote", `*You press the button, and 3 options appear.`);
				sender.Tell("Emote", `*[NEW USER]`);
				sender.Tell("Emote", `*[#@3!7 4E!6]`);
				sender.Tell("Emote", `*[LOGOUT]`);
				sender.Tell("Emote", `*The middle option seems to be broken, leaving only the (new) user and (logout) options.`);
				state.state = 2;
				return;
			}

			if (cmd === "!logout" && state.state === 2) {
				sender.Tell("Emote", `*You press the logout button, and the panel goes dark.`);
				sender.Tell("Emote", `*You are logged out.`);
				sender.Tell("Emote", `*You can log back in by (touch)ing the panel.`);
				state.state = 0;
				return;
			}

			if (cmd === "!new" && state.state === 2) {
				sender.Tell("Emote", `*You press the new user button, and the panel goes dark.`);
				state.state = 3;
				setTimeout(() => {
					sender.Tell("Emote", `*Suddenly, a large metal container appears in front of you, and the door swings open.`);
					sender.Tell("Emote", `*The box has a label on it that says "Stand Here". (stand) on the box?`);
				}, 3000);
				return;
			}
			if (cmd === "!stand" && state.state === 3) {
				if (!sender.IsNaked()) {
					sender.Tell("Emote", `*The machine beeps and nothing happens.....maybe you have too many clothes on?`);
					return;
				}
				const box = sender.Appearance.AddItem(AssetGet("ItemDevices", "FuturisticCrate"));
				if (!box) {
					logger.alert("Failed to add box to " + sender.Name);
					return;
				}
				sender.Tell("Emote", `*You step into the box.....nothing still happens.`);
				return;
			}

			sender.Tell("Whisper", `Unknown command.`);
			return;
		}
    }

    protected onCharacterOrderChanged(connection: API_Connector): void {
        if (connection.Player.IsRoomAdmin()) {
            void connection.Player.MoveToPos(0);
        }
    }
}