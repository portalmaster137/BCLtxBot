import { AddFileOutput, Connect, Init, LogLevel, SetConsoleOutput, logConfig, logger, AssetGet } from "bondage-club-bot-api";
import {USERNAME, PASSWORD} from "./secrets";

import * as fs from "fs";
import { run } from "node:test";
import { MainLogic } from "./MainLogic";

SetConsoleOutput(LogLevel.DEBUG);

const time = new Date();
const TimeSTR = `${time.getFullYear() % 100}${(time.getMonth() + 1).toString().padStart(2, "0")}${time.getDate().toString().padStart(2, "0")}_` +`${time.getHours().toString().padStart(2, "0")}${time.getMinutes().toString().padStart(2, "0")}`;
const Prefix = `${TimeSTR}_${process.pid}`

fs.mkdirSync('./data/logs', {recursive: true});
AddFileOutput(`./data/logs/${Prefix}_debug.log`, false, LogLevel.DEBUG);
AddFileOutput(`./data/logs/${Prefix}_error.log`, true, LogLevel.ALERT);

let conn: API_Connector | null = null;
let logic: MainLogic | null = null;

let ADMIN_LIST = [52412];
let BAN_LIST = [0];

async function main() {
    conn = await Connect(USERNAME, PASSWORD);

    logic = new MainLogic();
    conn.logic = logic;


    // @ts-ignore: dev
	global.AssetGet = AssetGet;
	// @ts-ignore: dev
	global.conn = conn;
	// @ts-ignore: dev
	//global.logic = testLogic;

    await conn.ChatRoomJoinOrCreate({
        Name: "Latex Conversion",
        Space: "X",
        Description: "[BOT] It's all slippery in here! (also come suggest ideas <3)",
		Background: "BDSMRoomPurple",
		Limit: 10,
		Private: true,
		Locked: false,
		Admin: [conn.Player.MemberNumber].concat(ADMIN_LIST),
		Ban: BAN_LIST,
		Game: "",
		BlockCategory: ["Leashing"]
    })

    logger.alert("Bot started");
}

Init().then(main, err => {
    logger.fatal("Init Rejected: " + err);
}).catch(err => {
    logger.fatal("Init Error: " + err);
})

logConfig.onFatal.push(()=>{
    conn?.disconnect();
    conn = null;
    logic = null;
})

process.once("SIGINT", ()=>{
    logger.fatal("Interrupted");
})